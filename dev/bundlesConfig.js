const fs = require('fs');


const bundles = [
    'Subpages/Homepage/Homepage',
];

const additional = {
};

const bundlesConfig = {
    Resources: [
        'jquery',
        'jquery-colorbox',
        'axios',
        'lodash',
        'tiny-slider',
        //'bootstrap'
    ],
    Main: ['./resources/scss/initial.scss'],
    ...additional,
};

const getBundleName = bundlePath => bundlePath.split('/').pop();

const containers = bundles.reduce((result, bundle) => {
    const path = `./views/${bundle}.js`;

    fs.existsSync(path)
        ? (result = { ...result, [getBundleName(bundle)]: path })
        : console.error(`${path} could not be bundled. Not found. `);

    return result;
}, {});

const config = { ...bundlesConfig, ...containers };

module.exports = config;
