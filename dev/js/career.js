import waterfall from './services/waterfall';
import init from './init';

import '../styles/career.scss';

const Career = () => {
  const createMasonry = () => {
    waterfall('.js-job-offers');
    window.addEventListener('resize', () => waterfall('.js-job-offers'));
  };
  createMasonry();
};

init()(Career);