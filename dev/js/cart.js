import init from './init';
import '../styles/cart.scss';
import StickySidebar from 'sticky-sidebar';
import { getWindowWidth } from './Helpers/helpers';

const Cart = () => {
    const runSticky = () => {
        if (getWindowWidth() > 991) {
            new StickySidebar('.sticky', {
                containerSelector: '#js-sticky-container',
                innerWrapperSelector: '.sidebar__inner',
                topSpacing: 100,
                bottomSpacing: 20,
            });
        }
    };

    const runChangeQuantity = () => {
        const changeQuantityButtons = document.querySelectorAll(
            '.js-cart-quantity'
        );

        const changeQuantityHandler = ({ currentTarget }) => {
            const quantityInput = currentTarget.parentElement.querySelector('input');
            const getQuantity = () => quantityInput.value;
            const incQuantity = () => {
                quantityInput.value = parseInt(getQuantity(), 10) + 1;
            };

            const decQuantity = () => {
                quantityInput.value =
                    getQuantity() > 1 ? parseInt(getQuantity(), 10) - 1 : getQuantity();
            };
            return currentTarget.classList.contains('inc')
                ? incQuantity()
                : decQuantity();
        };

        changeQuantityButtons.forEach(button =>
            button.addEventListener('click', changeQuantityHandler)
        );
    };

    runChangeQuantity();
    runSticky();
};

init()(Cart);
