import init from './init';
import '../styles/contact.scss';

const Contact = () => {
  const runMap = () => {
        const center = { lat: 50.884838, lng: 20.605434 };
        const markerIcon = '../images/marker.png';

        const map = new google.maps.Map(document.getElementById('google-map'), {
            center,
            zoom: 17,
        });

        new google.maps.Marker({
            position: center,
            map,
            title: 'Toflesz',
            icon: markerIcon,
            anchor: new google.maps.Point(66, 137),
        });
    };

    runMap();
};

init()(Contact);
