import init from './init';
import '../styles/faq.scss';
import Accordion from './services/accordion';

const Faq = () => {
    const runAccordion = () => {
        new Accordion({
            element: '.accordion',
            oneOpen: true,
        });
    };

    runAccordion();
};

init()(Faq);