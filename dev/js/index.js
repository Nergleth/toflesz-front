import { tns } from 'tiny-slider/src/tiny-slider';

import runInitialElements from './initial';
import '../styles/main.scss';

const Index = () => {
  const runCeramicSlider = () => {
    const slideCtrls = [...document.querySelectorAll('.js-hood-ctrl')];

    const slider = tns({
      container: '#js-hood-slider',
      items: 1,
      slideBy: 'page',
      autoplay: false,
      mode: 'gallery',
      animateOut: 'fade',
      controls: false,
      nav: false,
      autoplayButtonOutput: false,
      mouseDrag: true,
      viewportMax: '100%',
      responsive: {
        320: {
          items: 1
        }
      }
    });

    const changeCtrl = slideCtrl => {
      slideCtrls.forEach((th, index) => {
        th.classList.remove('active');
        index === slideCtrl && th.classList.add('active');
      });
    };

    slider.events.on('indexChanged', (info, eName) => {
      changeCtrl(info.displayIndex - 1);
    });

    const changeSlide = (e, index) => {
      slider.goTo(index);
    };

    slideCtrls.forEach((slideCtrl, index) =>
        slideCtrl.addEventListener('click', e => changeSlide(e, index))
    );
  };

  const runSlider = () => {
    const slideCtrls = [...document.querySelectorAll('.js-best-ctrl')];

    const slider = tns({
      container: '#js-best-slider',
      items: 1,
      slideBy: 'page',
      autoplay: false,
      controls: false,
      nav: false,
      autoplayButtonOutput: false,
      mouseDrag: true,
      viewportMax: '100%',
      responsive: {
        320: {
          items: 1
        }
      }
    });

    const changeCtrl = slideCtrl => {
      slideCtrls.forEach((th, index) => {
        th.classList.remove('active');
        index === slideCtrl && th.classList.add('active');
      });
    };

    slider.events.on('indexChanged', (info, eName) => {
      changeCtrl(info.displayIndex - 1);
    });

    const changeSlide = (e, index) => {
      slider.goTo(index);
    };

    slideCtrls.forEach((slideCtrl, index) =>
        slideCtrl.addEventListener('click', e => changeSlide(e, index))
    );
  };
  runInitialElements();
  runCeramicSlider();
  runSlider();
};

window.addEventListener('load', Index);
