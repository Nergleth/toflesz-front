import initialElements from './initial';

export default function () {

    return (mod) => {
        window.addEventListener('load', () => {
            initialElements();
            mod();
        });
    }
};