import noUiSlider from "nouislider";
import sumoselect from 'sumoselect';
import colorbox from 'jquery-colorbox';

import { scrollTop } from './Helpers/helpers';

const runInitialElements = () => {
    const onScroll = () => {
        const header = document.querySelector('#js-header');
        const catsHeader = document.querySelector('.cats-bar');
        const loadTop = scrollTop();

        const modifyBars = (action = 'remove') =>
            [header, catsHeader].forEach(bar =>
                bar && (action === 'add'
                ? bar.classList.add('scrolled')
                : bar.classList.remove('scrolled'))
            );

        const changeHeader = top =>
            top > 200
                ? modifyBars('add')
                : modifyBars();

        changeHeader(loadTop);

        const onWindowChangeHandler = () => {
            const top = scrollTop();
            changeHeader(top);
        };

        window.addEventListener('scroll', onWindowChangeHandler);
    };
    const runMenu = () => {
        const togglers = [...document.querySelectorAll('.js-menu-toggler')];
        const menu = document.querySelector('#js-menu');

        const toggleMenu = (e, close = false) => {
            if (close) {
                menu.classList.remove('active');
                return false;
            }
            menu.classList.toggle('active');
        };

        const onClickOutsideMenu = () => {
            window.addEventListener('click', e => {
                toggleMenu(e, true);
            });
            [menu, ...togglers].forEach(element =>
                element.addEventListener('click', e => e.stopPropagation())
            );
        };

        togglers.forEach(toggler => toggler.addEventListener('click', toggleMenu));
        onClickOutsideMenu();
    };

    const runSearchScreen = () => {
        $('.search-select').SumoSelect();
        const slider = document.getElementById('search-range');

        if (slider) {
            noUiSlider.create(slider, {
                start: [20, 8000],
                connect: true,
                tooltips: true,
                step: 10,
                range: {
                    min: 0,
                    max: 8000,
                },
                format: {
                    to(value) {
                        return `${value}zł`;
                    },
                    from(value) {
                        return value;
                    },
                },
            });
        }

        const handleSearchOpening = () => {
            const searchOpen = document.querySelector('#js-open-search');
            const searchClose = document.querySelector('#js-close-search');
            const searchScreen = document.querySelector('#js-search-screen');

            const toggleSearch = (open = false) =>
                open
                    ? searchScreen.classList.add('active')
                    : searchScreen.classList.remove('active');

            if (searchOpen && searchClose && searchScreen) {
                searchOpen.addEventListener('click', () => toggleSearch(true));
                searchClose.addEventListener('click', () => toggleSearch());
            }
        };

        handleSearchOpening();
    };
    const runSelect = () => {
        $('.select').SumoSelect();
    };

    runSelect();
    onScroll();
    runMenu();
    runSearchScreen();
};

export default runInitialElements;