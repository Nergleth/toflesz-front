import debounce from 'lodash/debounce';

import init from './init';
import '../styles/manuals.scss';

const Manuals = () => {
    const runSearch = () => {
    const searchInput = document.querySelector('#js-search-manual');
        const manualBoxes = [...document.querySelectorAll('.js-manual')];
        const searchSelect = document.querySelector('#cat');
        const manualInfos = [...document.querySelectorAll('.js-manual-info')];
        const manualsLists = [...document.querySelectorAll('.js-manuals')];

        const searchManuals = ({ target: { value } }) => {
            manualInfos.forEach(manual => manual.classList.remove('active'));

            const hasPhrase = ({ children: [title] }) =>
                title.innerText.toLowerCase().includes(value.toLowerCase());

            manualBoxes.filter(manual => {
                manual.parentElement.classList.remove('active');
                const containsPhrase = hasPhrase(manual);
                containsPhrase && manual.parentElement.classList.add('active');
                return containsPhrase;
            });

            manualsLists.forEach(list => {
                const manualInfo = list.querySelector('.js-manual-info');
                list.querySelectorAll('.active').length
                    ? manualInfo.classList.remove('active')
                    : manualInfo.classList.add('active');
            });
        };

        searchInput.addEventListener('keyup', debounce(searchManuals, 500));
        $(searchSelect).change(e => {
            [...manualsLists].forEach(manual => {
                manual.classList.remove('active');
                manual.classList.contains(`manuals-${e.target.value}`) &&
                manual.classList.add('active');
            });
        });
    };
    runSearch();
};

init()(Manuals);
