import init from './init';
import '../styles/news.scss';

const News = () => {
    const runColorbox = () => {
        $('.colorbox').colorbox({
            rel: 'gal',
            maxWidth: '80%',
            maxHeight: '80%'
        });
    };

    runColorbox();
};

init()(News);