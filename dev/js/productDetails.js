import init from './init';
import '../styles/productDetails.scss';
import { tns } from 'tiny-slider/src/tiny-slider';

const ProductDetails = () => {
    const runSlider = () => {
        const thumbs = [...document.querySelectorAll('.js-thumb')];

        const slider = tns({
            container: '.js-slider',
            items: 1,
            slideBy: 'page',
            autoplay: false,
            controls: false,
            nav: false,
            autoplayButtonOutput: false,
            mouseDrag: true,
            viewportMax: '100%',
            responsive: {
                320: {
                    items: 1
                }
            }
        });

        const changeThumb = thumb => {
            thumbs.forEach((th, index) => {
                th.classList.remove('active');
                index === thumb && th.classList.add('active');
            });
        };

        slider.events.on('indexChanged', (info, eName) => {
            changeThumb(info.displayIndex - 1);
        });

        const changeSlide = (e, index) => {
            slider.goTo(index);
        };

        thumbs.forEach((thumb, index) =>
            thumb.addEventListener('click', e => changeSlide(e, index))
        );
    };
    const runChangeQuantity = () => {
        const changeQuantityButtons = document.querySelectorAll('.js-amount');
        const quantityInput = document.querySelector('#js-quantity-input');

        const changeQuantityHandler = ({ currentTarget }) => {
            const getQuantity = () => quantityInput.value;
            const incQuantity = () => {
                quantityInput.value = parseInt(getQuantity(), 10) + 1;
            };

            const decQuantity = () => {
                quantityInput.value =
                    getQuantity() > 1 ? parseInt(getQuantity(), 10) - 1 : getQuantity();
            };
            return currentTarget.classList.contains('inc')
                ? incQuantity()
                : decQuantity();
        };

        changeQuantityButtons.forEach(button =>
            button.addEventListener('click', changeQuantityHandler)
        );
    };

    const runSharer = () => {
        const shareButton = document.querySelector('#js-share');
        const shareBox = document.querySelector('#js-socials');

        const toggleShareBox = () => shareBox.classList.toggle('active');
        shareButton.addEventListener('click', toggleShareBox);
        window.addEventListener('click', e => shareBox.classList.remove('active'));
        shareButton.addEventListener('click', e => e.stopPropagation());
    };

    runSlider();
    runChangeQuantity();
    runSharer();
};

init()(ProductDetails);
