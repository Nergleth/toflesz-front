import init from './init';
import '../styles/productList.scss';
import noUiSlider from "nouislider";

const ProductList = () => {
    const runRangeSlider = () => {
        const slider = document.getElementById('slider');

        if (slider) {
            noUiSlider.create(slider, {
                start: [20, 8000],
                connect: true,
                tooltips: true,
                step: 10,
                range: {
                    min: 0,
                    max: 8000,
                },
                format: {
                    to(value) {
                        return `${value}zł`;
                    },
                    from(value) {
                        return value;
                    },
                },
            });
        }
    };

    const runSelect = () => {
        $('.select').SumoSelect({
            placeholder: 'Wybierz',
        });
    };

    const runMobileSidebar = () => {
        const filterOpener = document.querySelector('#js-open-filters');
        const filters = document.querySelector('#js-filters');

        if (filterOpener && filters) {
            window.addEventListener('click', () =>
                filters.classList.remove('active')
            );
            filterOpener.addEventListener('click', e => {
                e.stopPropagation();
                filters.classList.toggle('active');
            });
            filters.addEventListener('click', e => e.stopPropagation());
        }
    };

    runRangeSlider();
    runSelect();
    runMobileSidebar();
};

init()(ProductList);
