/**
 * @fileOverview
 * @author Zoltan Toth
 * @version 1.1.0
 */

/**
 * @description
 * Vanilla JavaScript Accordion
 *
 * @class
 * @param {(string|Object)} options.element - HTML id of the accordion container
 *         or the DOM element.
 * @param {number} [options.openTab=1] - Start the accordion with this item opened.
 * @param {boolean} [options.oneOpen=false] - Only one tab can be opened at a time.
 */
const Accordion = (options)  => {
  const element =
    typeof options.element === 'string'
      ? document.querySelectorAll(options.element)
      : options.element;
  const { openTab } = options;
  const oneOpen = options.oneOpen || false;
  const titleClass = 'js-Accordion-title';
  const contentClass = 'js-Accordion-content';

  render();

  /**
   * Initial rendering of the accordion.
   */
  function render() {
    [...element].forEach(acc => {
      // attach classes to buttons and containers
      [].forEach.call(acc.querySelectorAll('button'), function (item) {
        item.classList.add(titleClass);
        item.nextElementSibling.classList.add(contentClass);
      });

      // attach only one click listener
      acc.addEventListener('click', onClick);
    });
    // sets the open tab - if defined
    if (openTab) {
      open(openTab);
    }
  }

  /**
   * Closes all accordion tabs.
   */
  function closeAll(clickedTarget) {
    const parent = clickedTarget.closest(options.element);
    const modifyItem = (classSelector, fn) => {
      [...parent.querySelectorAll(`.${classSelector}`)].forEach(fn);
    };

    modifyItem(titleClass,item => {
      item !== clickedTarget && item.classList.remove('active');
    });
    modifyItem(contentClass,item => {
      if (item.previousElementSibling !== clickedTarget) {
        item.style.height = 0;
        item.classList.remove('active');
      }
    });
  };

  /**
   * Toggles corresponding tab for each title clicked.
   *
   * @param {object} el - The content tab to show or hide.
   */
  function toggle(el) {
    // getting the height every time in case
    // the content was updated dynamically
    const height = el.scrollHeight;

    if (el.classList.contains('active')) {
      el.style.height = `${height}px`;
    } else {
      el.style.height = 0;
    }
  }


  /**
   * Handles clicks on the accordion.
   *
   * @param {object} e - Element the click occured on.
   */
  function onClick(e) {
    if (e.target.className.indexOf(titleClass) === -1) {
      return;
    }

    if (oneOpen) {
      e.target.classList.toggle('active');
      e.target.nextElementSibling.classList.toggle('active');
      closeAll(e.target);
    } else {
      e.target.classList.toggle('active');
      e.target.nextElementSibling.classList.toggle('active');
    }

    toggle(e.target.nextElementSibling);
  };

  /**
   * Returns the corresponding accordion content element by index.
   *
   * @param {number} n - Index of tab to return
   */
  function getTarget(n) {
    return element.querySelectorAll(`.${contentClass}`)[n - 1];
  }

  /**
   * Opens a tab by index.
   *
   * @param {number} n - Index of tab to open.
   *
   * @public
   */
  function open(n) {
    const target = getTarget(n);

    if (target) {
      if (oneOpen) closeAll();
      target.style.height = `${target.scrollHeight}px`;
    }
  }

  /**
   * Closes a tab by index.
   *
   * @param {number} n - Index of tab to close.
   *
   * @public
   */
  function close(n) {
    const target = getTarget(n);

    if (target) {
      target.style.height = 0;
    }
  }

  /**
   * Destroys the accordion.
   *
   * @public
   */
  function destroy() {
    element.removeEventListener('click', onClick);
  }

  return {
    open,
    close,
    destroy,
  };
};

export default Accordion;
