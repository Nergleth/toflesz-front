const tabs = () => {
  const tabsElement = document.querySelector('.js-tabs');
  const tabs = [...document.querySelectorAll('.js-tab')];
  const tabOpener = document.querySelector('#js-tabs-opener');
  const tabsContents = [...document.querySelectorAll('.js-tab-content')];

  const closeAllTabs = () =>
    tabsContents.map(
      tabContent =>
        new Promise(resolve => {
          tabContent.classList.remove('active');
          setTimeout(() => {
            tabContent.style.display = 'none';
            resolve('true');
          }, 300);
        })
    );

  const toggleMobileTabs = ({ target }) => [target, tabsElement].forEach(elem => elem.classList.toggle('show'));

  const openTabHandler = ({
    currentTarget: clickedTabsBlock,
    target: clickedTab,
  }) => {
    tabsElement.classList.remove('show');
    if (clickedTab.classList.contains('active')) {
      return false;
    }

    const tabToOpen = Array.prototype.indexOf.call(
      clickedTabsBlock.children,
      clickedTab
    );

    const openTab = () => {
      tabsContents[tabToOpen].style.display = 'block';
      tabsContents[tabToOpen].classList.add('active');
    };

    tabs.forEach(tabHeader => {
      tabHeader.classList.remove('active');
      clickedTab == tabHeader ? tabHeader.classList.add('active') : null;
    });
    Promise.all(closeAllTabs()).then(openTab);
  };

  tabsElement.addEventListener('click', openTabHandler);
  if (tabOpener) {
    tabOpener.addEventListener('click', toggleMobileTabs);
  } else {
    console.warn('No id="js-tabs-opener" element found!');
  }
};

export default tabs;
