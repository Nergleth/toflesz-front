import init from './init';
import '../styles/serwis.scss';
import flatpickr from "flatpickr";
import { Polish } from "flatpickr/dist/l10n/pl";
import tabs from "./services/tabs";

const Serwis = () => {
    const runDatepicker = () => {
        flatpickr('#dateofpurchase', {
            locale: Polish,
        });
    };

    runDatepicker();
    tabs();
};

init()(Serwis);