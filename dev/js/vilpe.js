import init from './init';
import '../styles/vilpe.scss';
import Accordion from './services/accordion';
import tabs from './services/tabs';

const Vilpe = () => {
    const runSpot = () => {
        const mask = document.querySelector('#js-spot-mask');
        const spot = document.querySelector('#js-spot');

        const playSpot = () => {
            const spotSrc = `${spot.getAttribute('src')}&autoplay=1`;
            spot.setAttribute('src', spotSrc);
            mask.classList.add('inactive');
        };

        mask.addEventListener('click', playSpot);
    };

    const runAccordion = () => {
        new Accordion({
            element: '.accordion',
            oneOpen: true,
        });
    };

    runAccordion();
    tabs();
    runSpot();
};

init()(Vilpe);