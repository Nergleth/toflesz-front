const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const autoprefixer = require('autoprefixer');

const webpack = require('webpack');
const prodConfig = require('./webpack.config.prod');

const mainConfig = {
  entry: {
    main: './js/index.js',
    career: './js/career.js',
    contact: './js/contact.js',
    cook: './js/cook.js',
    cooperation: './js/cooperation.js',
    productList: './js/productList.js',
    faq: './js/faq.js',
    manuals: './js/manuals.js',
    recipe: './js/recipe.js',
    serwis: './js/serwis.js',
    newsList: './js/newsList.js',
    news: './js/news.js',
    history: './js/history.js',
    technology: './js/technology.js',
    vilpe: './js/vilpe.js',
    productDetails: './js/productDetails.js',
    cart: './js/cart.js',
    financing: './js/financing.js',
    cartSummary: './js/cartSummary.js',
    cartDone: './js/cartDone.js',
  },
  output: {
    publicPath: '',
    path: path.resolve(__dirname, '../assets'),
    filename: path.join('../js/', '[name].bundle.js'),
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: '/node_modules/',
        use: {
          loader: 'babel-loader',
        },
      },
      {
        test: /\.css$/,
        exclude: '/node_modules/',
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: ['css-loader', 'resolve-url-loader'],
        }),
      },
      {
        test: /\.(sa|sc|c)ss$/,
        exclude: '/node_modules/',
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            {
              loader: 'css-loader',
              options: { minimize: true },
            },
            {
              loader: 'postcss-loader',
              options: {
                ident: 'postcss',
                plugins: () => [autoprefixer()],
              },
            },
            'resolve-url-loader',
            'sass-loader?sourceMap',
          ],
        }),
      },
      {
        test: /\.less$/,
        exclude: '/node_modules/',
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            {
              loader: 'css-loader',
              options: {
                importLoaders: 1,
              },
            },
            {
              loader: 'postcss-loader',
              options: {
                ident: 'postcss',
                plugins: () => [autoprefixer()],
              },
            },
            'less-loader',
          ],
        }),
      },
      {
        test: /\.(png|jpg|gif)$/,
        use: {
          loader: 'file-loader',
          options: {
            publicPath: '../assets/media/',
            outputPath: './media/',
            limit: 31200,
            esModule: false,
          },
        },
      },
      {
        test: /\.(woff(2)?|ttf|eot)(\?v=\d+\.\d+\.\d+)?$/,
        use: {
          loader: 'file-loader',
          options: {
            publicPath: '../assets/fonts/',
            outputPath: './fonts/',
            esModule: false,
          },
        },
      },
      {
        test: /\.svg$/,
        use: {
          loader: 'file-loader',
          options: {
            publicPath: '../assets/media/',
            outputPath: './media/',
            esModule: false,
          },
        },
      },
    ],
  },
  resolve: {
    alias: {
      jquery: path.resolve(__dirname, './node_modules/jquery/src/jquery'),
    },
    modules: [path.resolve(__dirname, './node_modules/'), './node_modules/'],
  },
  plugins: [
    /* new BundleAnalyzerPlugin({
             analyzerMode: 'static'
         }), */
    new ExtractTextPlugin(path.join('../css/', '[name].bundle.css')),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery',
      Promise: ['es6-promise', 'Promise'],
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      minChunks: Infinity,
    }),
  ],
};

const init = environment => {
  let plugins = [];

  if (environment === 'production') {
    mainConfig.devtool = 'source-map';
    plugins = [...mainConfig.plugins, ...prodConfig.plugins];
    mainConfig.plugins = plugins;
  }

  if (environment === 'development') {
    mainConfig.devtool = 'cheap-module-eval-source-map';
  }

  return mainConfig;
};

module.exports = function (environment) {
  return init(environment);
};
