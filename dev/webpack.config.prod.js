const ImageminPlugin = require('imagemin-webpack-plugin').default;
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const OptimizeCssPlugin = require('optimize-css-assets-webpack-plugin');
const webpack = require('webpack');

const prodConfig = {
    plugins: [
        new OptimizeCssPlugin({
            cssProcessorOptions: {
                discardComments: {removeAll: true },
                discardDuplicates: { removeAll: true },
            }
        }),
        new webpack.optimize.UglifyJsPlugin({
            minimize: true,
            beautify: false,
            comments: false,
            compress: {
               unused: true,
               dead_code: true,
               warnings: false,
               screw_ie8: true
            }
         }),
        new ImageminPlugin({ test: '../media/**',
            optipng: {
                optimizationLevel: 8
            }
        })
    ]
};

module.exports = prodConfig;